import 'package:dart_taobin/dart_taobin.dart' as dart_taobin;
import 'dart:io';

import 'taobin.dart';

void main(List<String> arguments) {
  var taobin = new Taobin();
  print("Welcome to Taobin beverage");
  taobin.showType();
  var chooseType = int.parse(stdin.readLineSync()!);
  taobin.showMenu(chooseType);
  var chooseMenu = int.parse(stdin.readLineSync()!);
  taobin.printMenuPrice(chooseType, chooseMenu);
  print("Please enter payment amount ");
  var money = int.parse(stdin.readLineSync()!);
  taobin.pay(money);
  print("Thank you");
}
